page 90101 "TCN_PruEntradaDatos"
{

    Caption = 'Prueba de Datos';
    PageType = Card;
    //me da igual el tipo de tabla, como solo va a ser para pedir datos
    SourceTable = integer;
    UsageCategory = Administration;

    actions
    {
        area(Creation)
        {
            action(prueba)
            {
                // lanzo la pagina desde el boton prueba o tb al abrirla pagina desde el
                //  el trigger OnOpenPage() al abrir la pagina
                trigger OnAction()

                var

                    xCaption: text;
                    xCaptionDecimal: Decimal;
                    xCaptiondate: date;
                    xnumero1: Integer;
                    Xnumero2: Integer;
                    xDate1: Date;
                    xDate2: Date;
                    xTexto1: text;
                    xtexto2: Text;

                begin

                    paTCN_EntradaDatosDinamica.SetTexto('Texto1', xTexto1);
                    paTCN_EntradaDatosDinamica.SetTexto('Texto2', xTexto2);
                    paTCN_EntradaDatosDinamica.SetDecimal('Numero1', xnumero1);
                    paTCN_EntradaDatosDinamica.SetDecimal('Numero2', xnumero2);
                    paTCN_EntradaDatosDinamica.SetDate('Fecha1', xDate1);
                    paTCN_EntradaDatosDinamica.SetDate('Fecha2', xDate2);


                    if paTCN_EntradaDatosDinamica.RunModal() in [Action::LookupOK, Action::OK] then begin

                        paTCN_EntradaDatosDinamica.GetTexto(xCaption, 1);
                        paTCN_EntradaDatosDinamica.GetTexto(xCaption, 2);
                        paTCN_EntradaDatosDinamica.GetDecimal(xCaptionDecimal, 1);
                        paTCN_EntradaDatosDinamica.GetDecimal(xCaptionDecimal, 2);
                        paTCN_EntradaDatosDinamica.GetDate(xCaptionDate, 1);
                        paTCN_EntradaDatosDinamica.GetDate(xCaptionDate, 2);

                    end;

                end;
            }
        }
    }
    var


        paTCN_EntradaDatosDinamica: Page TCN_EntradaDatosDinamica;
//ejercicio1
    trigger OnOpenPage()

    var

        xCaption: text;
        xCaptionDecimal: Decimal;
        xCaptiondate: date;
        xnumero1: Integer;
        Xnumero2: Integer;
        xDate1: Date;
        xDate2: Date;
        xTexto1: text;
        xtexto2: Text;


    begin

        paTCN_EntradaDatosDinamica.SetTexto('Texto1', xTexto1);
        paTCN_EntradaDatosDinamica.SetTexto('Texto2', xTexto2);
        paTCN_EntradaDatosDinamica.SetDecimal('Numero1', xnumero1);
        paTCN_EntradaDatosDinamica.SetDecimal('Numero2', xnumero2);
        paTCN_EntradaDatosDinamica.SetDate('Fecha1', xDate1);
        paTCN_EntradaDatosDinamica.SetDate('Fecha2', xDate2);


        if paTCN_EntradaDatosDinamica.RunModal() in [Action::LookupOK, Action::OK] then begin

            paTCN_EntradaDatosDinamica.GetTexto(xCaption, 1);
            paTCN_EntradaDatosDinamica.GetTexto(xCaption, 2);
            paTCN_EntradaDatosDinamica.GetDecimal(xCaptionDecimal, 1);
            paTCN_EntradaDatosDinamica.GetDecimal(xCaptionDecimal, 2);
            paTCN_EntradaDatosDinamica.GetDate(xCaptionDate, 1);
            paTCN_EntradaDatosDinamica.GetDate(xCaptionDate, 2);

        end;

    end;

}