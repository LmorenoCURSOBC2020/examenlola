page 90100 "TCN_EntradaDatosDinamica"
{

    Caption = 'Entrad de Datos Dinamica';
    PageType = Card;
    SourceTable = TCN_EntradaDatos;
    //UsageCategory = Administration;




    layout
    {
        area(Content)
        {
            group(General)
            {
                field(Txt01; mArrayTipoTexto[1])
                //aqui recogemos el valor q se le ha pasado por teclado

                {
                    ApplicationArea = All;
                    visible = xVisible1;
                    CaptionClass = StrSubstNo('3,%1', mArrayCaptionTexto[1]);
                    //aquiponemos como le vamos a llamar al campo
                }
                field(Txt02; mArrayTipoTexto[2])

                {
                    ApplicationArea = All;
                    Visible = xVisible2;
                    CaptionClass = StrSubstNo('3,%1', mArrayCaptionTexto[2]);
                }

                field(Num01; mArrayTipoDecimal[1])
                {
                    ApplicationArea = All;
                    Visible = xVisible3;
                    CaptionClass = StrSubstNo('3,%1', mArrayCaptionDecimal[1]);
                }
                field(Num02; mArrayTipoDecimal[2])
                {
                    ApplicationArea = All;
                    Visible = xVisible4;
                    CaptionClass = StrSubstNo('3,%1', mArrayCaptionDecimal[2]);
                }
                field(Date1; mArrayTipoDate[1])
                {
                    ApplicationArea = All;
                    Visible = xVisible5;
                    CaptionClass = StrSubstNo('3,%1', mArrayCaptionDate[1]);
                }
                field(Date2; mArrayTipoDate[2])
                {
                    ApplicationArea = All;
                    Visible = xVisible6;
                    CaptionClass = StrSubstNo('3,%1', mArrayCaptionDate[2]);
                }

            }
        }
    }
    var
        xVisible1: Boolean;
        xVisible2: Boolean;
        xVisible3: Boolean;
        xVisible4: Boolean;
        xVisible5: Boolean;
        xVisible6: Boolean;
        xContadorTexto: Integer;
        xContadorDate: Integer;
        xContadorDecimal: Integer;
        //por cada tipo de datos utizo un array de dos posiciones 
        mArrayCaptionTexto: array[2] of text;
        //para poner los captions de tipo texto 
        mArrayTipoTexto: array[2] of text;
        mArrayCaptionDecimal: array[2] of Text;
        mArrayTipoDecimal: array[2] of Decimal;
        // recoger los valores de entrada dependiendo del tipo de datos
        mArrayCaptionDate: array[2] of Text;
        mArrayTipoDate: array[2] of Date;


    trigger OnInit()// inicializo un conjunto de variables globales
    var
    begin
        xVisible1 := false;
        xVisible2 := false;
        xvisible3 := false;
        xVisible4 := false;
        xVisible5 := false;
        xvisible6 := false;
        xContadorTexto := 1;
        xContadorDate := 1;
        xContadorDecimal := 1;

    end;
    //hacemos tres funciones SET una para cada tipo de datos
    procedure SetTexto(pCaption: text; ptipo: text)
    var

    begin

        if xContadorTexto < 3 then begin

            mArrayTipoTexto[xContadorTexto] := ptipo;
            mArrayCaptionTexto[xContadorTexto] := pCaption;
            //aprovechando el contador doy visibilidad a las variables
            if xContadorTexto = 1 then begin
                xVisible1 := true;
            end;
            if xContadorTexto = 2 then begin
                xVisible2 := true;
            end;
            xContadorTexto += 1;
        end else begin
            xContadorTexto := 1;
        end;

    end;

    procedure SetDecimal(pCaption: text; ptipo: Decimal)
    var

    begin

        if xContadorDecimal < 3 then begin

            mArrayTipoDecimal[xContadorDecimal] := ptipo;
            mArrayCaptionDecimal[xContadorDecimal] := pCaption;
            if xContadorDecimal = 1 then begin
                xVisible3 := true;
            end;
            if xContadorDecimal = 2 then begin
                xVisible4 := true;
            end;
            xContadorDecimal += 1;
        end else begin
            xContadorDecimal := 1;
        end;

    end;

    procedure SetDate(pCaption: text; ptipo: Date)
    var

    begin

        if xContadorDate < 3 then begin

            mArrayTipoDate[xContadorDate] := ptipo;
            mArrayCaptionDate[xContadorDate] := pCaption;
            if xContadorDate = 1 then begin
                xVisible5 := true;
            end;
            if xContadorDate = 2 then begin
                xVisible6 := true;
            end;
            xContadorDate += 1;
        end else begin
            xContadorDate := 1;
        end;

    end;
    //hacemos tres funciones GET una para cada tipo de datos para 
    //la recogida de datos

    procedure GetTexto(var pTexto: text; pNumero: integer)
    var
    begin

        pTexto := mArrayTipoTexto[pnumero];

        //Message('valor texto recogido%1', pNumero, ' recogido'); son mensajes de prueba
    end;

    procedure GetDecimal(var pDecimal: Decimal; pNumero: integer)
    var
    begin

        pDecimal := mArrayTipoDecimal[pnumero];
        // Message('valor decimal recogido%1', pNumero, ' recogido');


    end;

    procedure GetDate(var pDate: Date; pNumero: integer)
    var
    begin

        pDate := mArrayTipoDate[pNumero];
        //  Message('valor fecha recogido%1', pNumero, ' recogido');

    end;



}
