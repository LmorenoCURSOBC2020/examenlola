table 90100 "TCN_EntradaDatos"
{
    Caption = 'TCN_EntradaDatos';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Txt01; Text[100])
        {
            Caption = 'Txt01';
            CaptionClass = '';
            DataClassification = ToBeClassified;

        }
        field(2; Txt02; Text[100])
        {
            Caption = 'Txt02';
            DataClassification = ToBeClassified;
        }
        field(3; Num01; Decimal)
        {
            Caption = 'Num01';
            DataClassification = ToBeClassified;
        }
        field(4; Num02; Decimal)
        {
            Caption = 'Num02';
            DataClassification = ToBeClassified;
        }
        field(5; Date1; Date)
        {
            Caption = 'Date1';
            DataClassification = ToBeClassified;
        }
        field(6; Date2; Date)
        {
            Caption = 'Date2';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; Txt01)
        {
            Clustered = true;
        }
    }
    // procedure SetTexto(pCaption: text; pTexto: Text)
    // begin

    // end;

    // procedure SetNumero(pCaption: text; pNumero: Decimal)
    // begin

    // end;

    // procedure SetFecha(pCaption: text; pFecha: Decimal)
    // begin

    // end;

    // procedure GetTexto(pCaption: text; pTexto: Text)
    // begin

    // end;

    // procedure GetNumero(pCaption: text; pNumero: Decimal)
    // begin

    // end;

    // procedure GetFecha(pCaption: text; pFecha: Decimal)
    // begin

    // end;
}
