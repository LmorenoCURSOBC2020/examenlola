codeunit 90100 "Tcn_Funciones"
{

    SingleInstance = true;
    

    var
        xlCodCliente: Text[20];
        //xlSeparador: text;
        xCodeCliente: code[20];
        rlcustomer: record customer;
        cliente: text;

    procedure PedirDatos(var pCaption: text; var pCaption2: text)
    var
        paTCN_EntradaDatosDinamica: page TCN_EntradaDatosDinamica;
        xlseparador: text;
        xlseparador2: text;

    begin
        paTCN_EntradaDatosDinamica.SetTexto('CodCliente', xlCodCliente);
        paTCN_EntradaDatosDinamica.SetTexto('Separador', xlSeparador);

        if paTCN_EntradaDatosDinamica.RunModal() in [Action::LookupOK, Action::OK] then begin

            paTCN_EntradaDatosDinamica.GetTexto(pCaption, 1);
            paTCN_EntradaDatosDinamica.GetTexto(pcaption2, 2);

        end;
    end;

    procedure ImportarDatos()
    var
        xlcliente: label 'No existe el cliente';
        xlSeparador: text;
        rlcustomer: record customer;
        rlSalesHeader: Record "Sales Header";
        rlSalesline: Record "Sales Line";
        xlSeleccion: integer;
        xlNumLinea: Integer;
        xlNumLineaPedido: integer;
        rlTemBlobTMP: Record TempBlob temporary;
        xlInStream: InStream;
        xlFichero: Text;
        xlLinea: Text;
        xlEsCabecera: boolean;
        xlEsLinea: boolean;
        xlCampo: Text;
        xlNumCampo: Integer;
        rlTempCommetline: Record "Comment Line" temporary;

        //xlFiltro: Text;
    begin
        PedirDatos(xlCodCliente, xlSeparador);
        Message('el cliente%1 existe%2', xlCodCliente, rlcustomer.Name);
        Message('el separador es%1', xlSeparador);
        if rlcustomer.get(xlCodCliente) then begin

            //Message('xlcodcliente%1', rlcustomer.name);
            if xlSeparador <>'' then begin
                //leer e introducir ficheros de texto desde fuera
                rlTemBlobTMP.Blob.CreateInStream((xlInStream));

                if (UploadIntoStream('Seleccione fichero', '',
               'Archivos de texto (*.txt,*.csv)|*.txt;*.csv|Todos los archivos(*.*)|*.*',
                xlFichero, xlInStream)) then begin

                    While not xlInStream.EOS do begin //mientras no sea fin de fichero
                        xlInStream.ReadText(xlLinea);
                        xlNumLinea += 1;
                        xlcampo := CopyStr(xllinea, 1, 1);
                        if xlcampo = 'C' then begin
                            xlEsCabecera := true;
                            xlEsLinea := false;
                        end;
                        if xlcampo = 'L' then begin
                            xlEsCabecera := false;
                            xlEsLinea := true;
                        end;

                        if xlEsCabecera then begin

                            rlSalesHeader.Init();


                            rlSalesHeader.Validate("Document Type", rlSalesHeader."Document Type"::Order);

                            if AsignarDatosCabeceraF(xlLinea, xlSeparador, rlSalesHeader) then begin
                                rlSalesHeader.Validate("Sell-to Customer No.", xlCodCliente);

                                //como se ha ejecutado pasa directamente a insertar
                                rlSalesHeader.Insert(true)// then begin//true cuando no son tablas temporales,
                            end else begin
                                rlTempCommetline.Init();
                                rlTempCommetline."Line No." := xlNumLinea;
                                xlNumLineaPedido := 1;

                                rlTempCommetline.Comment := CopyStr(StrSubstNo('la linea %1 devolvio el siguiente error %2', rlSalesHeader."No.", GetLastErrorText),
                                                                        1, MaxStrlen(rlTempCommetline.Comment));
                                rlTempCommetline.Insert(false);
                                clear(rlSalesHeader);

                            end;
                        end;
                    end;

                    if xlEsLinea then begin

                        rlSalesLine.Init();
                        rlSalesline.Validate("Document Type", rlSalesHeader."Document Type"::Order);
                        rlSalesline.Validate("Document No.", rlSalesHeader."No.");
                        rlSalesline.Validate("Sell-to Customer No.", xlCodCliente);
                        rlSalesline.validate("Line No.", xlNumLineaPedido);

                        if AsignarDatosLineaF(xlLinea, xlSeparador, rlSalesline) then begin
                            //como se ha ejecutado pasa directamente a insertar
                            rlSalesline.Insert(true);
                            clear(rlSalesline);
                            xlNumLineaPedido += 100;

                        end else begin
                            rlTempCommetline.Init();
                            rlTempCommetline."Line No." := xlNumLinea;

                            rlTempCommetline.Comment := CopyStr(StrSubstNo('la linea %1 devolvio el siguiente error %2', rlSalesline."Line No.", GetLastErrorText),
                                                                    1, MaxStrlen(rlTempCommetline.Comment));
                            rlTempCommetline.Insert(false);

                        end;
                    end;
                end;
                    
            end;
        end else begin
            Message('no sube fichero');
        end;
    end else begin
                Message('no hay separador');
            end;
    end else begin
            Message('no hay cliente');
        end;
        //cuenta si hay lineas en el fichero tmp
        if rlTempCommetline.Count = 0 then begin
            Message('proceso Finalizado');
        end else begin
            page.Run(0, rlTempCommetline);
        end;
    end;
                

local procedure PasarTextoDate(pTexto: Text) xSalida: Date //cambiar los textos a tipo fecha
    var
        xlDia: integer;
        xlMes: integer;
        xlY: integer;
        xFechaSalida: Date;
        xHoraSalida: Time;


    begin
        if pTexto = '' then begin
            xSalida := 0D;
        end else begin
            Evaluate(xlDia, CopyStr(pTexto, 1, 2));
            Evaluate(xlMes, CopyStr(pTexto, 3, 2));
            Evaluate(xlY, CopyStr(pTexto, 5, 4));
            xFechaSalida := DMY2Date(xlDia, xlMes, xlY);
        end;


    end;
    


    [TryFunction]
    local procedure AsignarDatosCabeceraF(pLinea: Text; pSeparador: text; var prSalesHeader: Record "Sales Header")
    var

        xlCampo: Text;
        xlNumCampo: Integer;

    begin
        xlNumCampo := 1;
        foreach xlCampo in pLinea.Split(pSeparador) do begin //el split el separador de ficheros aqui ;
            xlNumCampo += 1;

            case xlNumCampo of
                2:
                    begin

                        //prSalesHeader.Init();//inicializa, limpia, escribe  encima
                        prSalesHeader.Validate("Order Date", PasarTextoDate(xlCampo));

                    end;
                3:
                    begin

                        prSalesHeader.Validate("Document Date", PasarTextoDate(xlCampo));
                    end;
                // 3: este campo es el q pone en el reg, pero sales line aparece Document date
                //     begin

                //         prSalesHeader.Validate("Posting Date", PasarTextoDate(xlCampo));
                //     end;

                4:
                    begin
                        prSalesHeader.Validate("External Document No.", xlCampo);
                    end;

            end;
        end;
    end;


    [TryFunction]

    local procedure AsignarDatosLineaF(pLinea: Text; pSeparador: text; var prSalesLine: Record "Sales line")
    var

        xlCampo: Text;
        xlNumCampo: Integer;

    begin
        xlNumCampo := 1;
        foreach xlCampo in pLinea.Split(pSeparador) do begin //el split el separador de ficheros aqui ;
            xlNumCampo += 1;

            case xlNumCampo of
                2:
                    begin
                        if xlcampo = 'C' then begin
                            prSalesLine.validate(type, prSalesLine.Type::"G/L Account");
                        end;
                        if xlcampo = 'P' then begin
                            prSalesLine.validate(type, prSalesLine.Type::Item);
                        end;

                    end;
                3:
                    begin
                        prSalesLine.Validate("No.", CopyStr(xlCampo, 1, MaxStrLen(prSalesLine."No.")));


                    end;
                4:
                    begin


                        prSalesLine.Validate(Description, CopyStr(xlCampo, 1, MaxStrLen(prSalesLine.Description)));

                    end;
                5:
                    begin

                        evaluate(prSalesLine.Quantity, xlCampo);

                        prSalesLine.Validate(Quantity, prSalesLine.Quantity / 100);

                    end;

                6:
                    begin

                        evaluate(prSalesLine."Unit Price", xlCampo);

                        prSalesLine.Validate("Unit Price", prSalesLine."Unit Price" / 100);
                    end;

                7:
                    begin
                        evaluate(prSalesLine."Line Discount %", xlCampo);

                        prSalesLine.Validate("Line Discount %", prSalesLine."Line Discount %" / 100);
                    end;

            end;
        end;
    end;









}